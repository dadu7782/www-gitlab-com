---
layout: markdown_page
title: "Category Vision - Value Stream Management"
---

- TOC
{:toc}

## Value Stream Management
The enterprise software industry is quickly transitioning to an Agile DevOps delivery
model, where the focus is no longer only on getting bits and bytes out the door,
but instead on shipping iterative business value continuously to end users, with 
rapid customer feedback.

GitLab will be enhanced with value streams and analytics built natively into the 
application itself. [Customized workflows](https://gitlab.com/groups/gitlab-org/-/epics/424) 
will allow teams to define their own value streams, relevant to their own company, 
their own customers, and their own industry. GitLab will surface [VSM (value stream management) style metrics](https://gitlab.com/groups/gitlab-org/-/epics/229),
such as cycle time, lead time, and deployment frequency, aggregated and rolled up, 
per an individual team, across multiple teams within a department, or even across 
the entire organization. Since GitLab is the first single application for the entire 
DevOps lifecycle, [these metrics](https://gitlab.com/groups/gitlab-org/-/epics/313) 
will allow teams to quickly identify and pinpoint process gaps and areas needing 
urgent improvement.

See [upcoming planned improvements for value streams and analytics](https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=analytics).

## What's next & why
<!-- This is almost always sourced from the following sections, which describe top
priorites for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

The first step for building out Value Stream Management is calculating the cycle time between any two label events. See https://gitlab.com/gitlab-org/gitlab-ee/issues/10663.

## Competitive landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

The competitive landscape here is similar to that of [Agile Portfolio Management](https://gitlab.com/groups/gitlab-org/-/epics/667) in that the two product categories go hand-in-hand. A lot of portfolio management tools have in the recent past, been innovating on surfacing value streams to users. These includes VersionOne, Tasktop, and XebiaLabs. GitLab is unique since we have all DevOps stages in a single application, so we can more easily and accurately determine the actual stage times with little manual user input required, and surface them in a coherent manner.

## Analyst landscape
<!-- What analysts and/or thought leaders in the space talking about, and how we stay
relevant from their perspective.-->

We worked with Forrester and participated in the [Forrester New Wave: Value Stream Management Tools in 2018](https://about.gitlab.com/analysts/forrester-vsm/). The industry is leveraging a set of processes (that they have grouped together into what is commonly called "Value Stream Management" or "Value Stream Analytics") to focus large enterprises on delivering business value to customers at a high velocity. Whereas in the past, the industry may have focused on optimizing individual stages in a business process and the efficacy of those individual stages, value stream management is ultimately concerned with the end-to-end cycle time of delivering business value (and in the software world, that is often validated software features) to the end user. It is hyper-focused on the customer and their gained utility in using your company's product, as opposed to measuring bits and bytes that you ship. Furthermore, the focus is on end-to-end cycle time as the primary metric, as opposed to trying to retrofit or standardize on some abstract notion of customer business value. Agile has taught us that shipping in small iterations as quickly as possible allows organizations to quickly converge product features to a customer value optimum. This also offsets risk of working on the wrong thing, because the cost of shipping something "wrong" is one small iteration, and the benefit is actually _learning_ a bit more what the user wants or finds valuable. Therefore, value stream management uses end-to-end cycle time as primary metric, because it is ultimately a transferrable metric and can be used to compare organizations, and even compare across industries.

However, individual stages still _do_ matter in value stream management. In particular, once you have determined your organization's (or your team's) end-to-end cycle time as the baseline, you can then figure out how to reduce it to remain competitive. Here, individual stage times are critical because a director-level or even executive-level sponsor has the entire visibility of the value stream, and can see right away _where_ the inefficiencies lie. For example, if there is a lot of waste time (which is often idle/lead time) in the QA stage, the sponsor may elect to spend more resources in optimizing that stage further, through investments in headcount and/or automation/other technologies. So we see that individual stages and their times are important, but they serve as a means to optimize the end-to-end cycle time, which is ultimately what actually matters, because that is what actually impacts the customer, and drives your business.

GitLab is well-positioned to innovate in value stream management. Since our vision is to house _all_ stages of the DevOps lifecycle in a single application, managing all the stages, measuring their respective stage times, and measuring the end-to-end cycle time, and surfacing all that information to users, is much more natural and has potential for a much better experience for the end-user, as compared to other solutions on the market.

Our customers have said that they are interested in defining their own custom value streams and we also agree this is the future of the software industry as a whole, since different verticals have different risk profiles in different stages, and/or needs of stages, and even the ordering of said stages. So our first goal in value stream management is allowing customers to build out those custom workflows in the first place. See [this epic](https://gitlab.com/groups/gitlab-org/-/epics/364).

## Top Customer Success/Sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

There hasn't been significant demand for VSM as a particular issue/feature since it is new. But customers have said that they are interested in the feature. In particular, the first functionality would be measuring cycle time, as in [this issue](https://gitlab.com/gitlab-org/gitlab-ee/issues/10663_.

## Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

This is the same as above. See [issue #10663](https://gitlab.com/gitlab-org/gitlab-ee/issues/10663).

## Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

This is the same as above. See [issue #10663](https://gitlab.com/gitlab-org/gitlab-ee/issues/10663).

## Top Vision Item(s)
<!-- What's the most important thing to move your vision forward?-->

- MVC: [issue #10663](https://gitlab.com/gitlab-org/gitlab-ee/issues/10663).
- Rest of design is in [this epic](https://gitlab.com/groups/gitlab-org/-/epics/949).