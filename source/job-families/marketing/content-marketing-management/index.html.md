---
layout: job_family_page
title: "Content Marketing Management"
---

Managers in the Content Marketing team are prolific and creative content strategists.
They define GitLab’s content marketing strategy, and manage a world-class team of content
creators to expand GitLab's influence, awareness, subscribers, and leads.
While they are strong writers, editors, and creatives, their time is spent supporting
and growing the skills of their team. They own the delivery and results of Content Marketing
deliverables and OKRs. They are focused on improving results, productivity, and
operational efficiency. They must also collaborate across departments to
accomplish collaborative goals.

## Manager, Content Marketing

## Responsibilities

- Define, implement, and regularly iterate on GitLab's content marketing strategy.
- Develop and implement a content framework for managing content development, publishing, amplification, and measurement.
- Oversee GitLab's editorial, digital content, and social marketing initiatives.
- Build and expand audience reach and engagement via a strategic publishing plan.
- Increase newsletter subscribers and contribute to marketing pipeline.
- Collaborate with product marketing on positioning and messaging.
- Collaborate with Digital Programs on campaign strategy.
- Perform regular content gap analysis to idendify areas of success, improvement, and opportunity.
- Hire and manage a world class team of content marketers and creatives.
- Hold regular 1:1s with all members of the team

### Requirements

- Degree in marketing, journalism, communications or related field.
- 3-5 years experience in content marketing or journalism at an enterprise software company or news outlet.
- 2+ years experience managing a team of writers.
- Experience defining the high-level strategy and creating content plans based on research and data.
- A dual-minded approach: Highly creative and an excellent writer/editor but also process-driven, think scale, and rely on data to make decisions.
- Strong communication skills without a fear of over communication. This role will require effective collaboration and coordination across internal and external contributors.
- Obsessive about content quality not quantity.
- Regular reporting on content and channel performance.
- You share our [values](/handbook/values), and work in accordance with those values.
- [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#management-group)
- BONUS: A passion and strong understanding of the industry and our mission.

**NOTE** In the compensation calculator below, fill in "Manager" in the `Level` field for this role.

## Interview Process

- Screening call with recruiter
- Interview with Director of Corporate Marketing (manager)
- Interview with Content Marketer (report)
- Interview with Director of Product Marketing (collaborator)
- Interview with CMO

Additional details about our process can be found on our [hiring page](/handbook/hiring/).
