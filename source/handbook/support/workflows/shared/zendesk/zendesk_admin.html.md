---
layout: markdown_page
title: Zendesk Admin
category: Zendesk
---

### On this page
{:.no_toc}

- TOC
{:toc}

----

## Adding / Removing Agents in Zendesk

To add or remove "full" agents to our Zendesk account, contact Zendesk support through the email address contained in the Support vault on 1Password titled "Zendesk Account Manager". This makes sure that the request is routed more quickly than if it is just sent in via their generic support address. Since we are on an annual contract, changes typically require a purchase order to be signed by the [VP of Engineering](/job-families/vp-of-engineering), but the change request can be initiated by any current "full" agent.

## Zendesk settings

### Service Level Agreements set as Business Rules

Within Zendesk, [Service Level Agreements (SLA) policies are defined under Business Rules in the Admin console.](https://support.zendesk.com/hc/en-us/articles/204770038-Defining-and-using-SLA-policies-Professional-and-Enterprise-) 

Currently we have 8 different SLA policies, and each of those with 4 different Targets which are set depending on the _priority_  of the ticket which can be Urgent, High, Normal, or Low. 

At this stage, we have SLAs defined for First Reply Time (FRT) and Next Reply Time (NRT), all of them in Business Hours except for Emergencies, which are set in Calendar Hours. You can find more information about our Service Level Agreements in our [Support Page](https://about.gitlab.com/support/).

- **Emergency SLA** has a 30 minutes FRT for all priorities and 4 hours NRT for all priorities. 
- **Silver and Gold SLA**, & **Premium and Ultimate SLA**

    FRT: 
    - Urgent: 30m
    - High: 4h
    - Normal: 8h
    - Low: 24h

    NRT: 
    - Urgent: 4h
    - High: 4h
    - Normal: 8h
    - Low: 24h
- **Bronze SLA**, **Starter SLA**, & **GitHost SLA**

    FRT: 
    - Urgent: 24h
    - High: 24h
    - Normal: 24h
    - Low: 24h

    NRT: 
    - Urgent: 24h
    - High: 24h
    - Normal: 24h
    - Low: 24h
- **Accounts Receivable**, & **Upgrades and Renewals**

    FRT: 
    - Urgent: 4h
    - High: 16h
    - Normal: 24h
    - Low: 36h

    NRT: 
    - Urgent: 4h
    - High: 16h
    - Normal: 24h
    - Low: 36h


## Requesting a Zendesk API Token 

If you need to request a [Zendesk API Token](https://support.zendesk.com/hc/en-us/articles/231426867-Beginner-s-Guide-to-the-Zendesk-API), you can do so by submitting an [API Token Request Issue](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=New%20Access%20Request) (look for 'API Token Request' in the Dropdown list next to Title) with the title **Zendesk API Token Request**. This help us keep track of who has access to the token and manage token expiration and replacement. 

